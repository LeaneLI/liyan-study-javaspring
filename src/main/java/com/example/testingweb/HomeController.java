package com.example.testingweb;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
/*
Creat a simple application
 */
@Controller
public class HomeController {
    @RequestMapping
    public @ResponseBody String greeting(){
        return "Hello World";

    }
}
